from django.conf.urls import url, include

from .views import UserList, UserDetail
from .views import BlogPostList, BlogPostDetail, UserBlogPostList
from .views import PhotoList, PhotoDetail, BlogPostPhotoList

user_urls = [
    url(r'^(?P<username>[0-9a-zA-Z_-]+)/blogposts$', UserBlogPostList.as_view(), name='userblogpost-list'),
    url(r'^(?P<username>[0-9a-zA-Z_-]+)$', UserDetail.as_view(), name='user-detail'),
    url(r'^$', UserList.as_view(), name='user-list')
]

blogpost_urls = [
    url(r'^(?P<pk>\d+)/photos$', BlogPostPhotoList.as_view(), name='blogpostphoto-list'),
    url(r'^(?P<pk>\d+)$', BlogPostDetail.as_view(), name='blogpost-detail'),
    url(r'^$', BlogPostList.as_view(), name='blogpost-list')
]

photo_urls = [
    url(r'^(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', PhotoList.as_view(), name='photo-list')
]

urlpatterns = [
    url(r'^users', include(user_urls)),
    url(r'^blogposts', include(blogpost_urls)),
    url(r'^photos', include(photo_urls)),
]