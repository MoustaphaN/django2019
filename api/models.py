from django.db import models

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
	follerwers = models.ManyToManyField('user', related_name='followees', symmetrical=False)

class BlogPost(models.Model):
 	author = models.ForeignKey(User, on_delete = models.CASCADE, related_name= 'blogposts')
 	title = models.CharField(max_length= 255)
 	body = models.TextField(blank= True, null=True)


class Photo(models.Model):
	blogposts = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='photos')
	image = models.ImageField(upload_to="%Y/%m/%d")