from django.shortcuts import render
from rest_framework import generics, permissions

from .models import User, BlogPost, Photo
from .serializers import UserSerializer, BlogPostSerializer, PhotoSerializer
from .permissions import BlogPostAuthorCanEditPermission 


class UserMixin(object):
	model = User
	queryset = User.objects.all()
	serializer_class = UserSerializer

class UserList(UserMixin, generics.ListAPIView):
	permissions_classes = [
        permissions.AllowAny
	]


class UserDetail(UserMixin, generics.RetrieveAPIView):
	lookup_field = 'username'


class BlogPostMixin(object):
	model = BlogPost
	queryset = BlogPost.objects.all()
	serializer_class = BlogPostSerializer
	permissions_classes = [
        BlogPostAuthorCanEditPermission
	]
		
def perform_create(self, serializer):
	serializer.save(author=self.request.user)


class BlogPostList(BlogPostMixin, generics.ListCreateAPIView):
    pass

class BlogPostDetail(BlogPostMixin, generics.RetrieveUpdateDestroyAPIView):
    pass



class UserBlogPostList(generics.ListAPIView):
	model = BlogPost
	queryset = BlogPost.objects.all()
	serializer_class = BlogPostSerializer

	def get_queryset(self):
		queryset = super(UserBlogPostList, self).get_queryset()
		return queryset.filter(author_username=self.kwargs.get('username'))


class PhotoMixin(object):
	model = Photo
	queryset = Photo.objects.all()
	serializer_class = PhotoSerializer


class PhotoList(BlogPostMixin, generics.ListCreateAPIView):
	permissions_classes = [
		permissions.AllowAny
	]


class PhotoDetail(PhotoMixin, generics.RetrieveUpdateDestroyAPIView):
	permissions_classes = [
		permissions.AllowAny
	]


class BlogPostPhotoList(generics.ListAPIView):
	model = Photo
	queryset = Photo.objects.all()
	serializer_class = PhotoSerializer

	def get_queryset(self):
		queryset = super(BlogPostPhotoList, self).get_queryset()
		return queryset.filter(blogpost__pk=self.kwargs.get('pk'))