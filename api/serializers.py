from rest_framework import serializers

from .models import User, BlogPost, Photo



class UserSerializer(serializers.ModelSerializer):
	blogposts = serializers.HyperlinkedIdentityField(view_name='userblogpost-list', lookup_field='username')

	class Meta:
		model = User
		# fields = ('id', 'username', 'first_name', 'last_name', 'blogposts',)
		fields = '__all__'


class BlogPostSerializer(serializers.ModelSerializer):
	author = UserSerializer(required=False)
	photos = serializers.HyperlinkedIdentityField(view_name='blogpostphoto-list')

	def get_validation_exclusions(self, *args, **kwargs):

		exclusions = super(BlogPostSerializer, self).get_validation_exclusions(*args, **kwargs)
		return exclusions + ['author']
	class Meta:
		model = BlogPost
		fields = '__all__'


class PhotoSerializer(serializers.ModelSerializer):
	image = serializers.ReadOnlyField(source='image.url')

	class Meta:
		model = Photo
		fields = '__all__'