from django.contrib import admin

from .models import User, BlogPost, Photo


admin.site.register(User)
admin.site.register(BlogPost)
admin.site.register(Photo)