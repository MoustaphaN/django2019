from django.conf.urls import include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()


urlpatterns = [
    url(r'api/v1/', include('api.urls')),
    url(r'admin/', admin.site.urls),
]